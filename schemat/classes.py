import weakref
from schemat.util import public


###############################################################################
#                                   Formulas                                  #

"""
* Formulas (predicate logic formula)

A "formula" is essentially an equation, a recipe for how to combine a set of
functions and inputs (variables).  The formula and the result of evaluating the
formula are different things.

Formulas can be nested as long as the arg types are appropriate.  Internally,
formulas are represented like lisp sexps as nested tuples, where the first
element of each tuple is the function, the others are the arguments.  Any
argument that's a string with length one is a variable name.

The functions can be either logical operators (And, Or, Not) or Preds (predicate
functions, i.e. instances of the Pred class or a subclass), or they can be arbitrary
Python functions, such as the <= < == >= > operators (which are named le lt eq
ge gt, imported from the operator module for convenience).  (In future, possibly
quantified formulas too.)

Preds have a type (return type) that's one of:
-bool (the default), for conditions that are strictly true/false;
-fuzzy: a fuzzy bool, a value from -1.0 to 1.0, where 0.0 means unknown;
-float: a real number, e.g. temperature, weight;
-int: countable. Unlike float each rule that derives the fact increases its
 count;
-other custom types.

Multiple derived instances of a predicate are combined by combining their return
values, e.g. fuzzy values can be added non-linearly as the strength of evidence
increases.

So although a formula can evaluate to arbitrary values that are meaningless to
the engine (sometimes called 'terms'), the root function of a formula that's
used as part of a rule must always be a logical connective or a Pred.

When done in Python rather than the DSL, formulas are usually built up using the
[] operator as syntactic sugar, for example Above[obj, x] is equivalent to
(Above, obj, 'x').

** Evaluation

A variable is *free* if no value is provided for it (in an *environment* dict)

(The rest of this section describes implementation details, not part of the user
guide.)

A formula containing no free variables is called *closed* and can be evaluated.

A *reduced* form of a formula without connective has all variables substituted
(is closed) and all subformulas are replaced with their evaluations. Formulas
with connectives are reduced as follows:
-Not is removed, negating its arg result, or distributing over And/Or by de Morgan
-Or is shortcut evaluated to a single "true" argument
-And is preserved

The internal eval_form() function performs evaluation, and requires an
environment.  It does not just return the return value of the root function,
but rather a Fact (TODO).

** Facts

A Fact tells the result of evaluating a Pred, and is what is stored in as an
attribute (or relation) of an object. E.g. Flammable(Tree)=True.


"""

# Any length one
# Syntactic sugar for variables
x = 'x'
y = 'y'
z = 'z'

Not = 'Not'
Or = 'Or'
And = 'And'

# Operators < <= == >= > can be used to in the conditions of a rule
# e.g. Rule([gt, Temperature(x), 100], 
from operator import lt, le, eq, ge, gt

#REMOVE
# Term type codes (unimplemented) are as follows:
# T: a term, such as an object, function-with-args (including functional predicates), variable, or value.
# P: the name of a predicate, i.e. a functor
# X: a variable
## P/x: a predicate with x arguments, e.g. P/2
# V: a value
## I: an integer
# L: a list

def isvariable(term):
    "Whether a term in a formula is a variable"
    return isinstance(term, str)

#REMOVE
class Term:
    """
    A term is anything that can be used as an argument to a predicate.

    Recursively defined as:
    -a Variable
    -a Value
    -a function applied to a full set of arguments
    -a list of Terms
    """



class AppliedFunctor():

    def __getitem__(self, args):
        "Syntactic sugar to return a formula; see explanation under Formulas"
        if isinstance(args, tuple):
            return (self,) + args
        return (self, args)

    def __str__(self):
        return "{}({})".format(self[0], ', '.join(map(str, self[1])))

    def __repr__(self):
        return self.__class__.__name__ #+ self.    #repr((self[0],) + self[1])


# TODO
def eval_form(form, env):
    """Evaluate a formula:
    Evaluates the args (except when shortcut evaluating) and then the function, returning its result
    (which would usaully be a Pred return value like fuzzy or bool; an Object; or a list of objects.
    Returns a pair (reduced_form, result)
    """
    if not isinstance(form, list):
        return form, form   # Can this happen?

    # NOTE: this function is being written

    head = form[0]
    if head == Or or head == And:
        # Special case, don't evaluate all args
        value
        for arg in form[1:]:
            assert isinstance(arg, tuple), "%s only takes formulas as args, not %s" % (head, repr(arg))
            ret = eval_form(arg, env)
            if head == Or:
                value = max(value, ret.value)
                if value >= 1:
                    return value

    elif head == Not:
        assert len(args) == 1
        return args[0]

    elif callable(head):
        # Includes Pred instances and raw user (python) function

        args = []
        for arg in self[1]:
            if isinstance(arg, tuple):
                args.append(eval_form(arg, env))
            else:
                args.append(arg)

        print("%s: eval'ing %s %s" % (self.__class__.__name__, self[0], args))
        return head(args)

    else:
        raise "Unknown formula head %s, should be a callable (e.g. Pred) or connective"


###############################################################################
#                              Predicate values                               #



@public
class fuzzy(float):
    def __init__(self, val):
        assert 0. <= val <= 1.

    def __add__(self, rhs):
        assert 0. <= rhs <= 1.
        #float.__add__(self, rhs)
        ret = 1 - (1 - self) * (1 - rhs)
        return fuzzy(ret)

    def __str__(self):
        return "%g" % self
    def __repr__(self):
        return "fuzzy(%g)" % self



###############################################################################
#                                     Util                                    #

DefinedCtors = dict()

class SingletonConstructor:
    """Any class inheriting from this becomes a singleton factory,
    e.g. calling Class('tree') twice returns the same object, cached in DefinedCtors."""

    def __new__(cls, name, *args):
        if name in DefinedCtors:
            return DefinedCtors[name]
        ret = super().__new__(cls)
        DefinedCtors[name] = ret
        return ret

    # def __call__(self, *args):
    #     return self.ctor(self, *args)

    def __str__(self):
        return self.name


#REMOVE
class Func(SingletonConstructor, AppliedFunctor):

    # ctor = Term

    def __init__(self, name, nargs, function):
        self.name = name
        self.nargs = nargs
        self.func = function

    def __repr__(self):
        return f"Func('{self.name}', {self.nargs})"

    # obsolete syntax
    # def __call__(self, *args):
    #     return FuncTerm(self.function, args)


###############################################################################
#                                  Predicates                                 #



######

# Pred aka class Concept:

@public
class Pred(SingletonConstructor, AppliedFunctor):

    # The following constants can be passed as flags to the constructor

    # Automatic rules
    # c a class
    # .P a predicate (meaning a function with all arguments except its first provided)

    # Inheritable:
    #  c.P && o.isA(c) -> o.P
    # Predicates assumed inheritable by default unless they are marked either:
    KIND_ONLY = 1    # eg. "Cats are widespread."
    COLLECTIVE = 2   # a predicate about groups of objects, eg. "The books are sorted"
    # See also https://en.wikipedia.org/wiki/Predicate_(grammar)#Carlson_classes

    ONDEMAND = 4     # When

    # ctor = Fact

    def __init__(self, name, rettype = bool, argtypes = None, flags = 0, default = None):
        self.name = name
        assert isinstance(flags, int)
        if argtypes is None:
            argtypes = [Object]
        assert len(argtypes) >= 1, "Need at least one argument (an Object)"
        assert argtypes[0] == Object, "First argument must be an Object"

        self.argtypes = tuple(argtypes)
        self.rettype = rettype

        if default is None:
            if rettype == fuzzy:
                # The fact that there is no rule to derive something is evidence that it's false
                self.default = fuzzy(-0.7)
            elif rettype in (bool, int, float):
                self.default = rettype()  #False, 0, 0.0
            else:
                raise ValueError("Need explicit default value for Pred with rettype %s" % (rettype,))
        else:
            self.default = default

        self.flags = flags

        # inheritable: c.P && o.isA(c) -> o.P
        self.inheritable = True
        if flags & Pred.KIND_ONLY or flags & Pred.COLLECTIVE:
            self.inheritable = False

        # All Rules that can derive this predicate
        self.deriver_rules = RuleList(self, RuleList.ANTECEDENTS)
        # All Rules that can derive this predicate, for each of its arg positions
        self.arg_deriver_rules = [RuleList(self, RuleList.ANTECEDENTS) for arg in argtypes]
        # All Rules that have this predicate as a condition
        self.consequent_rules = RuleList(self, RuleList.CONSEQUENTS)


    def __repr__(self):
        return f"Pred({self.name}{self.argtypes} : {self.rettype})"

    def __getindex__(self, *args):
        """Syntactic sugar: instantiate a Fact"""
        print("call: ", self, args)
        return (self,) + args



#e.g.  _sorted = Pred("sorted", bool, [list], flags=COLLECTIVE)


def substitute_vars_in_form(form, env):
    pass

@public
class Fact():
    """
    An evaluated formula.
    A closed formula together with its value (result)
    """
    def __init__(self, formula, env, value):
        self.form = substitute_vars_in_form(formula, env)
        self.value = value


@public
class Class(Pred):
    """A special case of a predicate which is an is-a relation.
    For example Cat(x) can be read "x is-a Cat"."""

    def __init__(self, name, rettype = bool):
        if rettype == fuzzy:
            default = fuzzy(-1)
        else:
            default = False
        flags = 0
        super().__init__(name, rettype, [Object], flags, default)

    def __repr__(self):
        return f"Class('{self.name}')"


################################################################################


#REMOVE
class Value:
    def __init__(self, val):
        self.value = value


#REMOVE
class Var:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"Var('{self.name}')"

################################################################################
#                                    Rules                                     #


DEFINITIVE=1e6  # Special rule strength value. (aka final, conclusive, absolute)

@public
class Rule:
    def __init__(self, conditions, effects, strength = 0):
        self.conditions = conditions
        self.effects = effects
        self.twoway = False   # if and only if
        self.strength = strength

        Rulebase_add_rule(self)

    @classmethod
    def parse(self, *tokens, strength = None):
        """Parses a string of formulas and tokens such as (Tree[X], '->', Wooden[X]) into a Rule.
        There must be exactly one connective: '->', or '<->'
        ('<-' not supported, as it may be a bad idea)

        TODO (unimplemented):
        Automatically combines multiple formulas on the left- and right-hand side (LHS/RHS)
        of the connective into an And or Or according to the following defaults:
        ->
          LHS is And, RHS is a list of independent consquences (which is internally an And,
          but it's better not to think of it that way).
        <->
          LHS and RHS are And
        <-
          LHS must be a single formula, RHS is Or
        """
        lhs = []
        rhs = []
        onleft = True
        for tok in tokens:
            if tok in ('->', '<->', '<-'):
                assert onleft, "More than one logical connective not allowed; found " + tok
                assert tok != '<-', "<- not supported"
                connective = tok
                onleft = False
            elif onleft:
                lhs.append(tok)
            else:
                rhs.append(tok)

        assert onleft == False, "No logical connective (e.g. '->') found"

        # Add default
        #if connective == '->':
        def_lhs = And
        def_rhs = And

        if len(lhs) > 1:
            lhs = (def_lhs,) + tuple(lhs)
        if len(rhs) > 1:
            rhs = (def_rhs,) + tuple(rhs)

        if strength is None:
            strength = 5 if connective == '<->' else 0
        ret = Rule(lhs, rhs, strength)
        ret.twoway = (connective == '<->')
        return ret

    def __str__(self):
        return (", ".join(self.conditions) + " -> " + ", ".join(self.effects)
                + (self.strength and ("  [%+f]" % self.strength)))

    def __repr__(self):
        return f"Rule({self.conditions}, {self.effects}, {self.strength})"

    def check(self, world, env):
        """Return whether the conditions hold.
        env is a dict containing Variable assignments"""

    def do_effects(self):
        pass

    def apply(self):
        if self.check():
            self.do_effects()
            return True
        return False

    def forward_inference(self, env):
        # TODO: doesn't handle twoway
        reduced_form, result = eval_form(self.conditions, env)
        assert isinstance(result, (bool, fuzzy))

        if self.strength == DEFINITIVE:
            # Special case: if the conditions are false then effects are too
            pass # continue
        else:
            if result is False or abs(result) < 0.1:
                # Don't conclude anything. Uninformative fuzzy -0.1 to 0.1
                return

        assert isinstance(self.effects, tuple)
        assert self.effects[0] in (And, Not), "rule effect must be And/Not"

        if isinstance(condition, bool):
            pass #TODO

    # def check(self, world, env):
    #     """Returns whether the formula is true."""
    #     if isinstance(self.head, Pred):
    #         self.args[0]
    #         args
    #     #elif isinstance(self.head, 

    def solve_free_vars(self, env):
        # Free variables are implicit quantification over objects.
        # Some vars will be given, others need to be assigned. E.g.
        # Burning[X] and Touching[X, Y] -> Ignite[Y]
        # X has obj1 assigned to it, Y is free.
        # obj1 will be able to answer Touching[obj1, _].
        # Solution 1:
        #  Each Pred could keep list of all Objects o that have a Fact with o as first arg.
        #  Query those in turn (searching their lists of all facts; ideally Object would have a multimap from Pred -> Facts.
        #  (Efficiently implement the multimap as facts[p] -> either single Fact or a list of Facts)
        # Solution 2:
        #  Don't support free variables in the first argument in the condition.
        #  (But what about backwards inference? Same restriction on the effects seems too much)
        #  So above example: OK to search for Y given X, but not reverse.
        #have lists of all Objects Need to have lists of all objects with 
        vars = deduce_free_vars(self.conditions, env)


# Aliases
implies = Rule

rule = Rule.parse

# e.g.  rule(Tree[X], '->', Wooden[X])
# e.g.  implies(Tree[X], Wooden[X])




class RuleList():

    ANTECEDENTS = 0  # A list of rules can derive a predicate/fact
    CONSEQUENTS = 0  # A list of rules that provide consequences of a predicate/fact

    def __init__(self, owner, listtype):
        """owner: the Pred that owns this Rule. Used for error messages only.
        listtype: one of ANTECEDENTS, CONSEQUENTS"""
        self.rules = []
        self.owner = weakref.ref(owner)
        self.listtype = listtype

    def add(self, rule):
        if self.listtype == ANTECEDENTS:
            if len(self) > 0 and (self[0].strength == DEFINITIVE or rule.strength == DEFINITIVE):
                 print("Can only have one definitive rule that can derive %s, but have:\n%s\n%s"
                       % (self.owner(), self[0], rule))
                 raise ValueError
        self.rules.append(rule)
        self.rules.sort(key = (lambda rule: rule.strength), reverse = True)


def Rulebase_add_rule(rule):
    def add_direction(conditions, effects):

        for fact in effects:
            concept = fact[0]
            args = fact[1]
            for idx, arg in enumerate(args):
                if isinstance(arg, Pred):
                    arg.deriver_rules.add(rule)
                    arg.arg_deriver_rules[idx].add(rule)

        for fact in conditions:
            arg.consequent_rules.add(rule)

    add_direction(rule.conditions, rule.effects)
    if rule.twoway:
        add_direction(rule.effects, rule.conditions)



################################################################################

# NOTE: The following are old code, and likely need rewriting

@public
class World:
    def __init__(self):
        self.objects = set()
        # A World contains a special GLOBAL object which is what holds
        # the truth of 0-ary formulas or those without an object as first arg.
        self.GLOBAL = Object(self, 'GLOBAL')

    def _add_obj(self, obj):
        """Add an Object to the world. You don't need to call this, it's
        called automatically by Object.__init__()."""
        self.objects.add(obj)

    def add_fact(self, form, value = None):
        """
        form must be a reduced formula.
        value defaults to True, or whatever is the right type.

        Facts are stored in the following way:
        -the first arg is removed as it becomes implicit, as the fact is stored in that Object
        -remaining args are replaced with weakrefs
        -the result is appended
        """
        obj = form[1]
        obj._add_fact(form, value)

@public
class Object:
    def __init__(self, world, name, classes_and_attrs = []):
        """Create an object in a world with an initial set of Classes it belongs to, or facts about it
        Each item in classes_and_attrs is passed to add().
        """
        self.world = world
        self.name = name
        self.attrs = set()
        world._add_obj(self)
        for attr in classes_and_attrs:
            self.add(attr)
        #self.attr = set(attr[self] for attr in classes_and_preds)
        self.modified = False

    def __str__(self):
        return self.name

    def __repr__(self):
        return "Object" + repr((self.name,) + tuple(self.attrs))

    # TODO
    def check(self, args):
        """Returns whether the object has an attribute."""
        assert args[1] == self, "Wrong Object asked for truth of a formula"
        return args in self.attrs

    def _add_form(self, form, value = None):
        if value is None:
            value = form[0].rettype(True)
        assert form[1] is self or form[1]
        attr = (form[0],) + tuple(weakref.ref(arg) for arg in form[2:]) + (value,)
        print("adding attr %s to %s" % (attr, self))
        self.attrs.add(attr)
        self.modified = True

    def add(self, attr):
        """Add a fact/attribute to this object. (Alternative to calling World.add_fact().)
        'attr' should be either:
        -A name of a (possibly new) predicate of one arg
        -A Pred with one arg, such as a Class
        -A formula, such as Tree[x], to set true. The first arg should be a variable, which will be overwritten.
        -A pair (formula, value) giving a fact
        """
        if isinstance(attr, str):
            attr = Pred(attr)
        #assert isinstance(attr, Pred) or isinstance(attr, tuple)
        if isinstance(attr, Pred):
            assert len(attr.argtypes) == 1
            self._add_form((attr, self))
        elif isinstance(attr, tuple):
            if isinstance(attr[0], tuple):
                assert len(attr) == 2
                self._add_form(attr[0], attr[1])
            else:
                self._add_form(attr, None)
        else:
            raise ValueError("%s is not a Pred, predicate name, fact, or (fact, value) pair")

    # TODO
    def compute_attr(self):
        """Returns dict of all """
        ret = self.attrs
        for attr in ret:
            if isinstance(attr, Class):
                pass
