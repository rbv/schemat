#!/usr/bin/env python3

from schemat.classes import *

def run():
    world = World()

    Tree = Class("Tree")
    assert Tree == Class("Tree")

    form = Tree[x]
    print(form)

    # TODO: relations between classes don't devolve to class members yet
    livesIn = Pred('livesIn', bool, [Object, Class])

    Entity = Class("Entity")
    monkey = Object(world, 'monkey', [Entity, 'Mammal', livesIn[x, Tree]])
    monkey.add('alive')
    monkey.add(Pred('humanoid'))
    print(repr(monkey))


run()
